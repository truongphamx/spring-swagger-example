package com.truongpx.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value="Product", description="Defines a product")
public class ProductDTO {
    @ApiModelProperty(value = "The product's ID")
    private Integer id;
    @ApiModelProperty(value = "The product's name")
    private String name;
}
