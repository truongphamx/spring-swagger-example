package com.truongpx.controller;

import com.truongpx.dto.ErrorResponse;
import com.truongpx.dto.ProductDTO;
import com.truongpx.exception.NotFoundException;
import com.truongpx.exception.UnexpectedErrorException;
import io.swagger.annotations.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/products")
@Slf4j
@Api(tags = "Products resource")
public class ProductController {

    @ApiOperation(
            value = "API Create Product",
            notes = "API to create a new product",
            response = ProductDTO.class
    )
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Successfully created product", response = ProductDTO.class),
            @ApiResponse(code = 500, message = "Failed to create the product", response = ErrorResponse.class)
    })
    @PostMapping
    public ResponseEntity<ProductDTO> createProduct(
            @ApiParam(value = "Product to be created", required = true) @RequestBody ProductDTO product) {
        if (product.getName() == "Product 1") {
            return new ResponseEntity<>(new ProductDTO(999, "Product 1"), HttpStatus.CREATED);
        }
        throw new UnexpectedErrorException("Could not find product with name: " + product.getName());
    }

    @ApiOperation(
            value = "API Get Product",
            notes = "API to get product by its ID",
            response = ProductDTO.class
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully return product", response = ProductDTO.class),
            @ApiResponse(code = 404, message = "Could not find the product", response = ErrorResponse.class)
    })
    @GetMapping("/{productId}")
    public ResponseEntity<ProductDTO> getProduct(
            @ApiParam(value = "Product ID", required = true) @PathVariable("productId") Integer productId) {
        if (productId == 1) {
            return new ResponseEntity<>(new ProductDTO(1, "Product 1"), HttpStatus.OK);
        }
        throw new NotFoundException("Could not find product with ID: " + productId);
    }

}
